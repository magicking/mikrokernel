#ifndef PIC_H_
# define PIC_H_

#define PIC_MASTER_A (0x20)
#define PIC_MASTER_B (0x21)
#define PIC_SLAVE_A  (0xA0)
#define PIC_SLAVE_B  (0xA1)

#define PIC_ICW1               (0x01 << 4)
#define PIC_ICW1_ICW4_PRESENT  (0x01     )
#define PIC_ICW1_SINGLE        (0x01 << 1)
#define PIC_ICW1_LEVEL_TRIGGER (0x01 << 3)

#define PIC_ICW2(x) (((x) >> 3) << 3)

#define PIC_ICW4 (0x01)

#define PIC_MASTER_BASE 0x40
#define PIC_SLAVE_BASE  0x50

void init_pic();
void ack_master();
void ack_slave();

#endif /* !PIC_H_ */
