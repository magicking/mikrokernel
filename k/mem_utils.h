#ifndef MEM_UTILS_H_
# define MEM_UTILS_H_

#include "types.h"

void *memset(void *s, u32 c, u32 n);

#endif /* !MEM_UTILS_H_ */
