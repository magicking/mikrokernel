#ifndef SCREEN_H_
# define SCREEN_H_

#include "types.h"

#define SCREEN_FB 0x0b8000
#define COLUMNS 80
#define LINES 25

void screen_clear(u8 colour);
void printk(u8 colour, const s8* s);

#endif /* !SCREEN_H_ */
