#ifndef GDT_H_
# define GDT_H_

#include "types.h"

#define SEG_CODE      ((1 << 4) | (1 << 3))
#define SEG_DATA      (1 << 4)

#define SEG_ACCESS    (1)

/* DATA SEGMENT */
#define SEG_WRITE     (1 << 1)
#define SEG_EXPAND    (1 << 2)

/* CODE SEGMENT */
#define SEG_READ      (1 << 1)
#define SEG_CONFORM   (1 << 2)

#define SEG_RING0     (0)
#define SEG_RING1     (1 << 5)
#define SEG_RING2     (2 << 5)
#define SEG_RING3     (3 << 5)

#define SEG_PRESENT   (1 << 7)
#define SEG_AVL       (1 << 8)
#define SEG_DB_32     (1 << 10)
#define SEG_GRANUL    (1 << 11)

typedef struct
{
  u16	limit_l;
  u16	base_l;
  u8	base_m;
  uns	type	: 4;
  uns	s	: 1;
  uns	dpl	: 2;
  uns	p	: 1;
  uns	limit_h	: 4;
  uns	avl	: 1;
  uns	l	: 1;
  uns	d_b	: 1;
  uns	g	: 1;
  u8	base_h;
} __attribute__((packed)) gdt_entry;

void init_gdt();

#endif /* !GDT_H_ */
