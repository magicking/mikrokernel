#ifndef IDT_H_
# define IDT_H_

#include "types.h"

#define INT_TASK32  (0x05)
#define INT_INTR32  (0x0E)
#define INT_TRAP32  (0x0F)

#define INT_RING0   (0)
#define INT_RING1   (1 << 5)
#define INT_RING2   (2 << 5)
#define INT_RING3   (3 << 5)

#define INT_PRESENT (1 << 7)

typedef struct
{
  u16	addr_l;
  u16	segment;
  u8	reserved;    /* ZERO */
  union
  {
    struct
    {
      uns	type	: 4;
      uns	s	: 1; /* ZERO */
      uns	dpl	: 2;
      uns	p	: 1;
    } __attribute__((packed)) flags;
    u8 flags_u;
  };
  u16	addr_h;
} __attribute__((packed)) idt_entry;

typedef struct
{
  u32 ds;
  u32 es;
  u32 fs;
  u32 gs;
  u32 edi;
  u32 esi;
  u32 ebp;
  u32 esp;
  u32 ebx;
  u32 edx;
  u32 ecx;
  u32 eax;
  u32 int_num;
  u32 err_code;
  u32 eip;
  u32 cs;
  u32 eflags;
  /* Pushed in case of privilege change */
  u32 esp_prev;
  u32 ss;
} __attribute((packed)) regs_dump_int;


void init_idt();

#endif /* !IDT_H_ */
