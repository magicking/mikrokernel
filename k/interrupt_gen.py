#!/usr/bin/env python

asm_file = "int_asm.def"

int_error = {}
int_error[8] = True
int_error[9] = True
int_error[10] = True
int_error[11] = True
int_error[12] = True
int_error[13] = True
int_error[14] = True
int_error[17] = True

int_asm = "ASM_STUB_DEF(%d, %s)\n"

with open(asm_file, 'w') as f:
  for i in range(256):
    if not i in int_error:
      s = "push $0"
    else:
      s = "nop"
    f.write(int_asm % (i, s))
