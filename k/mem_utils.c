#include "mem_utils.h"

#include "types.h"

void *memset(void *s, u32 c, u32 n)
{
  for (u32 i = 0; i < n; i++)
    ((u8*)s)[i] = (u8)c;

  return s;
}
