#include "pic.h"
#include "asm_helpers.h"
#include "types.h"

void init_pic()
{
  /* ICW1 */
  outb(PIC_MASTER_A,
    PIC_ICW1 | PIC_ICW1_ICW4_PRESENT);
  outb(PIC_SLAVE_A,
    PIC_ICW1 | PIC_ICW1_ICW4_PRESENT);

  /* ICW2 */
  outb(PIC_MASTER_B, PIC_ICW2(PIC_MASTER_BASE));
  outb(PIC_SLAVE_B, PIC_ICW2(PIC_SLAVE_BASE));

  /* ICW3 */
  outb(PIC_MASTER_B, 0x04);
  outb(PIC_SLAVE_B,  0x02);

  /* ICW4 */
  outb(PIC_MASTER_B, PIC_ICW4);
  outb(PIC_SLAVE_B,  PIC_ICW4);
}

void ack_master()
{
  outb(PIC_MASTER_A, 0x20);
}

void ack_slave()
{
  outb(PIC_MASTER_A, 0x20);
  outb(PIC_SLAVE_A,  0x20);
}
