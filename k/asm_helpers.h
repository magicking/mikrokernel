#ifndef ASM_HELPERS_H_
# define ASM_HELPERS_H_

#include "types.h"

void outb(u16 port, u8 value);
u8    inb(u16 port);

#endif /* !ASM_HELPERS_H_ */
