#include "types.h"

#include "screen.h"

static u32 xpos = 0;
static u32 ypos = 0;

void screen_clear(u8 colour)
{
  s8* const fb = (void*) SCREEN_FB;

  for (u32 i = 0; i < COLUMNS * LINES; i++)
  {
    fb[i * 2] = ' ';
    fb[i * 2 + 1] = colour;
  }
  xpos = ypos = 0;
}

static void putchar(u8 colour, u32 c)
{
  s8* const fb = (void*) SCREEN_FB;

  switch (c)
  {
    case '\n':
      ypos++;
    case '\r':
      xpos = 0;
    break;
    default:
      fb[(xpos + ypos * COLUMNS) * 2] = (u8)c;
      fb[(xpos + ypos * COLUMNS) * 2 + 1] = colour;
      xpos++;
      if (xpos >= COLUMNS)
      {
        ypos++;
        xpos = 0;
      }
    break;
  }

  if (ypos >= LINES)
    ypos = 0;
}

void printk(u8 colour, const s8* s)
{
  for (u32 i = 0; s[i] != 0 && i < COLUMNS * LINES; i++)
    putchar(colour, s[i]);
}
