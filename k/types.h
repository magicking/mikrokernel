#ifndef TYPES_H_
# define TYPES_H_

typedef unsigned	uns;
typedef signed		sig;

typedef unsigned char	u8;
typedef char		s8;

typedef unsigned short	u16;
typedef short		s16;

typedef unsigned int	u32;
typedef int		s32;

struct pseudo_ptr
{
  u16 limit;
  u32 addr;
} __attribute__((packed));

#endif /* !TYPES_H_ */
