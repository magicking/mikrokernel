#include "types.h"
#include "mem_utils.h"

#include "gdt.h"

static gdt_entry  gdt_desc[3];
struct pseudo_ptr gdt_ptr;

static void set_gdt_entry(gdt_entry* e, u32 addr, u32 limit, u16 flags)
{
  e->base_l = addr & 0x0FFFF;
  e->base_m = (addr >> 16) & 0x0FF;
  e->base_h = (addr >> 24) & 0x0FF;

  e->limit_l = limit & 0x0FFFF;
  e->limit_h = (limit >> 16) & 0x0F;

  e->type = flags & 0x0F;
  e->s    = (flags >> 4) & 0x1;
  e->dpl  = (flags >> 5) & 0x3;
  e->p    = (flags >> 7) & 0x1;

  e->avl  = (flags >> 8) & 0x1;
/*  e->l    = (flags >> 9) & 0x1; */ /* 64b only */
  e->d_b  = (flags >> 10) & 0x1;
  e->g    = (flags >> 11) & 0x1;
}

static void gdt_flush()
{
  __asm__ (
    "lgdt gdt_ptr    \n"
    "mov $0x10, %%ax \n"
    "mov %%ax, %%ds  \n"
    "mov %%ax, %%es  \n"
    "mov %%ax, %%fs  \n"
    "mov %%ax, %%gs  \n"
    "mov %%ax, %%ss  \n"
    "jmp out%=       \n"
    "out%=:          \n"
    "push $0x08      \n"
    "push $end%=     \n"
    "lret            \n"
    "end%=:          \n"
    ::: "eax");
}

void init_gdt()
{
  memset(&gdt_desc, 0, sizeof (gdt_desc));

  set_gdt_entry(&gdt_desc[1], 0, 0xFFFFFFFF,
  SEG_CODE | SEG_READ | SEG_GRANUL | SEG_DB_32 | SEG_PRESENT);
  set_gdt_entry(&gdt_desc[2], 0, 0xFFFFFFFF,
  SEG_DATA | SEG_WRITE | SEG_GRANUL | SEG_DB_32 | SEG_PRESENT);

  gdt_ptr.limit = sizeof (gdt_desc) - 1;
  gdt_ptr.addr = (u32)&gdt_desc;

  gdt_flush();
}
