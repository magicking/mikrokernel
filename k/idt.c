#include "types.h"

#include "idt.h"
#include "pic.h"

#define ASM_STUB_DEF(int_num, instr)   \
  void int_stub##int_num (void);
#include "int_asm.def"
#undef ASM_STUB_DEF

static idt_entry  idt_desc[256];
static struct pseudo_ptr idt_ptr;

static void set_idt_entry(idt_entry* e, u32 addr, u8 flags)
{
  e->addr_l = (u16)(addr & 0x0FFFF);
  e->addr_h = (u16)((addr >> 16) & 0x0FFFF);
  e->segment = 0x8;
  e->flags_u = flags;
  e->reserved = 0;
}

static void idt_flush()
{
  __asm__ (
    "lidt idt_ptr\n"
    "sti\n"
    :::);
}

void init_idt()
{
#define ASM_STUB_DEF(int_num, instr)                                        \
  set_idt_entry(&idt_desc[(int_num)], (u32)int_stub##int_num, INT_INTR32 | INT_RING0 | INT_PRESENT);
#include "int_asm.def"
  idt_ptr.addr = (u32)&idt_desc;
  /* We are already in protected mode therefore we do not need
     to set real mode IDT */
  idt_ptr.limit = (sizeof (idt_desc)) - 1;
  init_pic();
  idt_flush();
}

void idt_handler(regs_dump_int* regs)
{
  (void)regs; /* Todo print an error with int number */

  if (PIC_MASTER_BASE <= regs->int_num && regs->int_num < PIC_MASTER_BASE + 8)
  {
    ack_master();
    return;
  }
  if (PIC_SLAVE_BASE <= regs->int_num && regs->int_num < PIC_SLAVE_BASE + 8)
  {
    ack_slave();
    return;
  }
  while(1);
}
