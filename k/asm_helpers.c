#include "types.h"

#include "asm_helpers.h"

inline void outb(u16 port, u8 value)
{
  __asm__ ("outb %0,%1" :: "r" (value), "r" (port));
}

inline u8 inb(u16 port)
{
  u8 value;

  __asm__ ("inb %1,%0" : "=r" (value) : "Nd" (port));

  return value;
}
